import { Injectable } from '@angular/core';
import {AngularFireDatabase,AngularFireList} from 'angularfire2/database';
import { Contacts } from './contact.model';
@Injectable({
  providedIn: 'root'
})

export class ContactService {
//property to data from firebase
contacts: AngularFireList<any>;

selectedcontact: Contacts=new Contacts();
  constructor(private database: AngularFireDatabase) {
    
   }
   
   getcontacts(){
     this.contacts = this.database.list('/contacts');
     return this.contacts;
    }
 
    insertdata(contactt:Contacts){
    this.contacts.push({
    firs_name:contactt.first_name,
    email:contactt.email
    });
  }
    updateinfo(contactt:Contacts){
    this.contacts.update(contactt.$key,{
    first_name:contactt.first_name,
    email:contactt.email,
    phone:contactt.phone
  });
 }
}
