export class Contacts {
    $key:string;
    first_name:string;
    email:string;
    gender:string;
    id:number;
    image:string;
    phone:string;
}